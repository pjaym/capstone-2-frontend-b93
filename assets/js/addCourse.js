let formSubmit =  document.querySelector("#createCourse");
let token = localStorage.getItem("token");
let modalAlert = document.querySelector("#modalAlert"); 
let modalTitle = document.querySelector("#staticBackdropLabel"); 
let modalMsg = document.querySelector("#modalMsg");

if(!token || token === 'false') {

	window.location.replace('../index.html')
}

formSubmit.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = document.querySelector("#courseName").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	let coursePrice = document.querySelector("#coursePrice").value;

	fetch(`${apiUrl}/api/courses`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data == true){
			$('#modalAlert').modal("show")	
			modalTitle.innerHTML = "Add a Course";
		    modalMsg.innerHTML = "Course added successfully"; 	
				
			// alert("Course successfully added!");
			// window.location.replace("./courses.html");
		}else {

			alert("Something went wrong.");
		}
	})

})