// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "get" method returns the value of the key passed in as an argument
let courseId = params.get('courseId');
// let isActive = params.get('isActive');
let isActive = (!params.get('isActive') || params.get('isActive') == "false") ? true : false;
let token = localStorage.getItem('token');
let modalMsg = document.querySelector("#modalMsg");
let modalTitle = document.querySelector("#staticBackdropLabel"); 
let delBtnColor = params.get("delBtnColor"); 

if(!token || token === 'false') {

	window.location.replace('../index.html')

}else {

	fetch(`${apiUrl}/api/courses/${courseId}`, {
	    method: 'DELETE',
	    headers: {
	    	'Content-Type': 'application/json',
	        'Authorization': `Bearer ${token}`
	    },
	    body: JSON.stringify({
	    	isActive: isActive
	    })
	})
	.then(res => res.json())
	.then(data => {

		//console.log(data);

		if(data === true){

			// Delete course successful
		    // Redirect to courses page
		    console.log(isActive)
		    if(isActive == false) {

		    	$('#modalAlert').modal("show")		    	
		    	modalTitle.innerHTML = "Disable Course";
		    	modalMsg.innerHTML = "Course disabled successfully";
		    
		    }else {

		    	$('#modalAlert').modal("show")
		    	modalTitle.innerHTML = "Enable Course";
		    	modalMsg.innerHTML = "Course enabled successfully";
				
		    }
		    //window.location.replace("./courses.html");
		    // alert(isActive);

		} else {

		    // Error in deleting a course
		    //alert("something went wrong");
		    $('#modalAlert').modal("show")
		    modalTitle.innerHTML = "Enable/Disable Course";
	    	modalMsg.innerHTML = "Something went wrong. Please refresh the page.";
			

		}

	})
}

