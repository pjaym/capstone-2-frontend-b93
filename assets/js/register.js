// selects the register form
let registerForm = document.querySelector("#registerUser");
let alert = document.querySelector("#alert");

registerForm.addEventListener("submit", (e) => {

	// prevents page redirection/reload
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#email").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNumber.length === 11)){

		// fetch('url', {options})
		fetch(`${apiUrl}/api/users/email-exists`, {
			method: 'POST', 
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			//console.log(data);

			if(data === false) {

				fetch(`${apiUrl}/api/users`, {
					method: 'POST',
					headers: {
						'Content-Type': "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber

					})
				})
				.then(res => res.json())
				.then(data => {

					//console.log(data);

					if(data === true) {

						$('#modalAlert').modal("show");
						$(document).ready((e) => {
						  $('#modalAlert').on('hidden.bs.modal', (e) => {
						    window.location.replace("./login.html")
							})
						})

						// Redirect to login
						//window.location.replace("./login.html");
						// console.log(true);

					}else {
						//alert("Something went wrong.")
						alert.innerHTML = 

							`
								<div class="alert alert-dark alert-dismissible fade show" role="alert">
								  Something went wrong. Please refresh refresh the page.
								</div>
							
							`
						window.setTimeout(() => { $(".alert").remove() }, 2000);
					}

				})


			}else {

				//alert("Duplicate email found.");
				alert.innerHTML = 

					`
						<div class="alert alert-dark alert-dismissible fade show" role="alert">
						  Duplicate email found.
						</div>
					
					`
				window.setTimeout(() => { $(".alert").remove() }, 2000);

			}


		})		
	
	}else {

		if(mobileNumber.length !== 11) {

			//alert("Mobile Number must be 11 digits.");
			alert.innerHTML = 

				`
					<div class="alert alert-dark alert-dismissible fade show" role="alert">
					  Mobile Number must be 11 digits.
					</div>
				
				`
			window.setTimeout(() => { $(".alert").remove() }, 2000);
		
		} 

		if(password1 !== password2) {

			//alert("Password did not match.");
			alert.innerHTML = 

				`
					<div class="alert alert-dark alert-dismissible fade show" role="alert">
					  Password did not match.
					</div>
				
				`
			window.setTimeout(() => { $(".alert").remove() }, 2000);

		}else {

			//alert("Something went wrong. Please try again");
			alert.innerHTML = 

				`
					<div class="alert alert-dark alert-dismissible fade show" role="alert">
					  Something went wrong. Please try again
					</div>
				
				`
			window.setTimeout(() => { $(".alert").remove() }, 2000);

		}

	}
})




// JSON.strinngify
// {email: email}
// "{email: email}"