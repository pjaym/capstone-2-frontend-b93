let adminUser = localStorage.getItem("isAdmin");
let userId = localStorage.getItem("id");
let cardFooter;
let modalButton  = document.querySelector("#adminButton");

if (adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

}else {

	modalButton.innerHTML =

	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-dark">
				Add Course
			</a>
		</div>
	`
}


fetch(`${apiUrl}/api/courses`)
.then(res => res.json())
.then(data => {

	// console.log(data);

	// Creates a variable that will restore the data to be rendered
	let courseData;

	if (data.length < 1) {

		courseData = "No courses available";
		
	}else  {
		
		
		courseData = data.map(course => {

				if(adminUser == "false" || !adminUser) {

				//console.log(course.enrollees);
				let findUser = {userId: userId};
				let found = course.enrollees.find(user => {
			      if (user.userId === findUser.userId) {
			      	return true;
			      } 
			    })

				let enrollBtn;
				let disabled;
				let btnColor;

				if(found) {

					enrollBtn = "Enrolled";
					disabled = "disabled";
					btnColor = "btn-secondary";

				}else {
					disabled = "";
					enrollBtn = "Select course";
					btnColor = "btn-dark";
				}

				cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn text-white btn-block editButton ${btnColor}">
							${enrollBtn}
						</a>
					`
					if(!course.isActive || course.isActive == "false") { return (""); }				
				
			}else {

				delBtn = (!course.isActive || course.isActive == "false") ? "Enable Course" : "Disable Course";

				delBtnColor = (!course.isActive || course.isActive == "false") ? "btn-secondary" : "bg-lightRed";

				cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn bg-darkBlack text-white btn-block editButton">
							View Enrollees
						</a>

						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-dark text-white btn-block editButton">
							Edit
						</a>

						<a href="./deleteCourse.html?courseId=${course._id}&isActive=${course.isActive}&delBtnColor=${delBtnColor}" value="${course._id}" value="${course.isActive}" value="${delBtnColor}" class="btn ${delBtnColor} text-white btn-block deleteButton" id="delBtn">
							${delBtn}
						</a>
					`
			}

			// let usersEnrolledCount = (course.enrollees.length < 1) ? ("") : course.enrollees.length;
			return (
				`
				<div class="col-md-4 my-3">
					<div class="card h-100">
						<div class="card-body bg-light">
							<p class="card-text text-right align-self-top txt-lightBlack"><strong>
								₱ ${course.price}
							</strong></p>
							<h5 class=card-title>
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>							
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
						<div class="card-footer">
							<p class="card-text text-left">
							<i class="fas fa-user-friends"></i>
								+ ${course.enrollees.length} enrolled
							</p>	
						</div>
					</div>
				</div>

				`
				)
		// Replaces the comma's in the array with an empty string
		}).join("");
	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})

