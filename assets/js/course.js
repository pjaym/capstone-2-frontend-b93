// "window.location.search" returns the query strinng part of the URL
console.log(window.location.search);

// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "has" method checks if the courseId key exists in the URL query string
// The method returns true if the key exists
console.log(params.has('courseId'));

// The "get" method returns the value of the key passed in as an argument
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');
let userId = localStorage.getItem('id');

// console.log(isAdmin);

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector("#enrollContainer");
let enrolleesHeader = document.querySelector("#enrolleesHeader");
let usersEnrolledContainer = document.querySelector('#usersEnrolledContainer');
let dataLoader = document.querySelector('.dataLoader');
let modalTitle = document.querySelector("#staticBackdropLabel"); 
let modalMsg = document.querySelector("#modalMsg");
let hide = document.querySelector("#hide"); 

if(!token || token == null) {
	
	window.location.replace('./register.html')
}



fetch(`${apiUrl}/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	// console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = 
	`
		<p class="secondary">
			Price: ₱ <span id="coursePrice"> ${data.price}</span> 
		</p>
	`
	dataLoader.innerHTML = 
	`

		<div class="col-md-12 dataLoader">
			
		</div>

	`

	if (!isAdmin || isAdmin == 'false') {

		hide.innerHTML = "";

		//console.log(data.enrollees)

		let findUser = {userId: userId};
		let found = data.enrollees.find(user => {
	      if (user.userId === findUser.userId) {
	      	return true;
	      } 
	    })

	    // console.log(found)
		// let enrollBtn = (found) ? "You are already enrolled" : "Enroll"
		let enrollBtn;
		let disabled;

		if(found) {

			enrollBtn = "Enrolled";
			disabled = "disabled";
			btnColor = "btn-secondary"

		}else {
			disabled = "";
			enrollBtn = "Enroll";
			btnColor = "btn-dark"
		}

		enrollContainer.innerHTML = 
		`	
			<button id="enrollButton" class="btn btn-block ${btnColor}" ${disabled}>
				${enrollBtn}
			</button>

		`

		usersEnrolledContainer.innerHTML = ``

		document.querySelector("#enrollButton").addEventListener("click", () => {

			document.querySelector("#enrollButton").disabled = true;

			if(!token || token == 'false') {

				window.location.replace('./register.html')

			}

			if (found) {

				// alert("You are already enrolled in this course");
				$('#modalAlert').modal("show")
				modalTitle.innerHTML = "Oooops...";
				modalMsg.innerHTML = "You are already enrolled in this course";

			}else{

				fetch(`${apiUrl}/api/users/enroll`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true) {

						$('#modalAlert').modal("show")
						modalTitle.innerHTML = "Thank you for enrolling!";
						modalMsg.innerHTML = "See you in class!";

						//window.location.replace("./courses.html");

					}else {

						// alert("Something went wrong");
						$('#modalAlert').modal("show")
						modalTitle.innerHTML = "Error occurred";
						modalMsg.innerHTML = "Something went wrong";
					}
				})
			}
		})

	}else {

		courseName.innerHTML = 

			`
				<td id="name">${data.name}</td>

			`
		courseDesc.innerHTML = 

			`
				<td id="email">${data.description}</td>

			`
		data.status = true ? "Active" : "Not Active";

		status.innerHTML = 

			`
				<td id="mobileNo">${data.status}</td>

			`

								
		usersEnrolledContainer.innerHTML = 

			`
				<div id='users' class="row"></div>

			`
			if(data.enrollees.length < 1) {

				enrolleesHeader.innerHTML = "No enrollees available!"
			}

		let users = document.querySelector('#users');

		usersData = data.enrollees.forEach(userData => { 			

			// console.log(userData.userId);

			fetch(`${apiUrl}/api/users/${userData.userId}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				}					
			})
			.then(res => res.json())
			.then(data => {
				
				// console.log(data);

				users.innerHTML += 

				`
					<div class="col-12 col-md-6">
					  	<div class="card my-2">
						    <div class="card-body border-left">
						      <h4 class="card-title"><span><i class="fas fa-user-graduate"></i></span> ${data.firstName} ${data.lastName}</h4>
						      <p class="card-text text-muted">${data.email}</p>
						      <p class="card-text"><small class="text-muted">${new Date(userData.enrolledOn).toDateString()}</small></p>
						  	</div>
						</div>
					</div>
				`

			})

		})
	}
})

/*
<tr>
<td>${data.firstName} ${data.lastName}</td>
<td>${new Date(userData.enrolledOn).toDateString()}</td>
</tr>

<div class="card-body">
				  	<h5 class="text-center"> 
				  		<i class="fas fa-user-friends"></i> + ${data.enrollees.length} Enrolled
				  	</h5> 
				    <table class="table table-bordered">
				    	<thead>
						    <tr>
						      <th scope="col">Name</th>
						      <th scope="col">Enrolled On</th>						     
						    </tr>
						</thead>
					  	<tbody id='users'>								  
					  	</tbody>
					</table>
				  </div>

*/