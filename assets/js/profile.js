let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');
let userId = localStorage.getItem('id');

let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let mobileNo = document.querySelector("#mobileNo");

let profileContainer = document.querySelector('#profileContainer');
let formSubmit = document.querySelector('#editUser');
let editUserBtn = document.querySelector('#editUserBtn');


if (!token || token === null){

	window.location.replace("./login.html");

}else {

	fetch(`${apiUrl}/api/users/details`, {
		method: 'GET',
		headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}	
	})	
	.then(res => res.json())
	.then(data => {

		// console.log(data);

		firstName.value = firstName.placeholder = data.firstName;
		lastName.value = lastName.placeholder = data.lastName;
		email.value = email.placeholder = data.email;
		mobileNo.value = mobileNo.placeholder = data.mobileNo;
		password1.value = password1.placeholder = data.password1;
		password2.value = password2.placeholder = data.password2;

		formSubmit.addEventListener("submit", (e) => {

			if(editUserBtn.innerText === "Edit Profile") {

				e.preventDefault();

				mobileNo.disabled = lastName.disabled = firstName.disabled = false;

				editUserBtn.innerText = "Save Profile";

			}else {

				e.preventDefault();

				let firstName = document.querySelector("#firstName");
				let lastName = document.querySelector("#lastName");
				let mobileNo = document.querySelector("#mobileNo");

				console.log(lastName.disabled)

				fetch(`${apiUrl}/api/users/${userId}`, {
					method: 'PUT',
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({

						firstName: firstName.value,
						lastName: lastName.value,
						mobileNo: mobileNo.value,
						userId: userId
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);
					// alert("Successfully saved!")
					mobileNo.disabled = lastName.disabled = firstName.disabled = true;
					

				})
				
				editUserBtn.innerText = "Edit Profile";
				$('#modalAlert').modal("show");
			}

		})
		
		isAdmin = isAdmin === 'true' ?  (profileContainer.innerHTML = "") :	
		profileContainer.innerHTML = 

			`
				<div id="enrollments" class="text-left row">
					
				</div>

			`

		let enrollments = document.querySelector('#enrollments');

		let courseName = document.querySelector('#courseName');
		let courseDesc = document.querySelector('#courseDesc');
		let enrolledOn = document.querySelector('#enrolledOn');
		
		data.enrollments.forEach(courseData => { 

			fetch(`${apiUrl}/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {
				
				enrollments.innerHTML += 

				`
				
				<div class="col-12 col-md-4">
				  	<div class="card my-2">
					    <div class="card-body border-left">
					      <h4 class="card-title text-left"><span><i class="fas fa-atlas"></i></span> ${data.name}</h4>
					      <p class="card-text text-left">Instructor: James Doe</p>
					      <p class="card-text text-left"><small class="text-muted">${new Date(courseData.enrolledOn).toDateString()}</small></p>
					  	</div>
					</div>
				</div>
						
		
				`

			})
			

		})

	})

}



/*<div class="card">
<div class="card-body">
<h5 class="text-center">Courses Enrolled</h5>
<table class="table table-dark table-borderless">				 
<tbody id='enrollments'></tbody>
</table>
</div>
</div>

<tr>
	<td>${data.name}</td>
	<td>${new Date(courseData.enrolledOn).toDateString()}</td>
	<td>${courseData.status}</td>
</tr>

<div class="card-header" id="courseName">				   
</div>
<div class="card-body">
<blockquote class="blockquote mb-0" id="courseDesc">
<p></p>
<footer class="blockquote-footer" id="enrolledOn"></footer>
</blockquote>
</div>

<div class="card-body my-3">
<blockquote class="blockquote mb-0">
<p>${data.description}</p>
<footer class="card-text"></footer>
</blockquote>

<div class="card-header card-title my-1">
<div class="col-md-12 profile-">
<i class="fas fa-atlas"></i>
<span><h5 class="text-left">${data.name}</h5></span>
<span><p class="text-left">Enrolled on ${new Date(courseData.enrolledOn).toDateString()}</p></span>
<span><h5 class="text-right">${data.name}</h5></span>	
</div>						  
</div>*/