// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "get" method returns the value of the key passed in as an argument
let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let name = document.querySelector("#courseName");
let price = document.querySelector("#coursePrice");
let description = document.querySelector("#courseDescription");


if(!token || token === 'false') {

    window.location.replace('../index.html')
}

fetch(`${apiUrl}/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data);

	// Assign the retrieved data as placeholders and the current value of the input fields
	name.placeholder = data.name;
	price.placeholder = data.price;
	description.placeholder = data.description;
	name.value = data.name;
	price.value = data.price;
	description.value = data.description;

})

document.querySelector("#editCourse").addEventListener( "submit", (e) => {

	e.preventDefault();

	let courseName = name.value;
	let courseDescription = description.value;
	let coursePrice = price.value;

	fetch(`${apiUrl}/api/courses`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            courseId: courseId,
            name: courseName,
            description: courseDescription,
            price: coursePrice
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	console.log(data);

    	if(data === true){

    		// Update course successful
    	    // Redirect to courses page
    	    // window.location.replace("./courses.html");

    	}else{

    	    // Error in updating a course
    	    alert("something went wrong");

    	}

    })

})
