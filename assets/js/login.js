// selects the register form
let loginForm = document.querySelector("#logInUser");
let alert = document.querySelector("#alert");

loginForm = addEventListener("submit", (e) => {

	// prevents page redirection/reload
	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password  = document.querySelector("#password").value;

	console.log(email);
	console.log(password);

	if (email == "" || password == "") {

		alert.innerHTML = 

			`
				<div class="alert alert-dark alert-dismissible fade show" role="alert">
				  Please input your email and/or password.
				</div>
			
			`
		window.setTimeout(() => { $(".alert").remove() }, 2000);

	}else {

		fetch(`${apiUrl}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(!data || data === false) {

				alert.innerHTML = 

				`
					<div class="alert alert-dark alert-dismissible fade show" role="alert">
					  Invalid username and/or password. Please try again.
					</div>
				
				`
				window.setTimeout(() => { $(".alert").remove() }, 2000);

			}

			if (data.accessToken) {

				// Store  JWT in local storage
				localStorage.setItem('token', data.accessToken)

				fetch(`${apiUrl}/api/users/details`, {
				headers: {
					'Authorization': `Bearer ${data.accessToken}`
				}
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					// Store the user's id and isAdmin properties in the local storage
					localStorage.setItem("id", data._id);
					localStorage.setItem("isAdmin", data.isAdmin);
					window.location.replace("./courses.html");
				})

			}else {

			}
		})
	}
	
})


/*localStorage {
	"token" : "sgjhkfsdgfsdgfsdgd"
}
*/